use crate::meta;
use crate::EKind;
use crate::ENode;
use crate::EType;

#[derive(Debug, Clone, PartialEq)]
pub struct And {
    args: Vec<ENode>,
}
impl And {
    pub fn new_enode(mut args: Vec<ENode>) -> Result<ENode, ENode> {
        match args.len() {
            0 => {
                args.push(meta::Placeholder::new_enode(String::from("Missing arg")));
                args.push(meta::Placeholder::new_enode(String::from("Missing arg")));

                Err(ENode::BooleanAnd(And { args }))
            }
            1 => {
                args.push(meta::Placeholder::new_enode(String::from("Missing arg")));

                Err(ENode::BooleanAnd(And { args }))
            }
            _ => Ok(ENode::BooleanAnd(And { args })),
        }
    }
}
impl EType for And {
    fn args(&self) -> Option<&[ENode]> {
        Some(&self.args)
    }

    fn eval(&self) -> ENode {
        let mut args_eval: Vec<ENode> = Vec::new();

        for a in &self.args {
            let a_eval = a.eval();

            match a_eval {
                ENode::BooleanValue(v) if v.as_bool() => continue,
                ENode::BooleanValue(_) => return Value::new_enode(false),
                _ => args_eval.push(a_eval),
            }
        }

        match args_eval.len() {
            0 => Value::new_enode(true),
            1 => args_eval.pop().unwrap(),
            _ => And::new_enode(args_eval).unwrap(),
        }
    }

    fn kind(&self) -> EKind {
        EKind::PureFunction
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct False {}
impl False {
    pub fn new_enode() -> ENode {
        ENode::BooleanFalse(False {})
    }
}
impl EType for False {
    fn args(&self) -> Option<&[ENode]> {
        None
    }

    fn eval(&self) -> ENode {
        Value::new_enode(false)
    }

    fn kind(&self) -> EKind {
        EKind::PureFunction
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Not {
    arg: Box<[ENode; 1]>,
}
impl Not {
    pub fn new_enode(arg: ENode) -> ENode {
        ENode::BooleanNot(Not {
            arg: Box::new([arg]),
        })
    }
}
impl EType for Not {
    fn args(&self) -> Option<&[ENode]> {
        Some(self.arg.as_ref())
    }

    fn eval(&self) -> ENode {
        let arg_eval = self.arg[0].eval();

        match arg_eval {
            ENode::BooleanValue(v) if v.as_bool() => Value::new_enode(false),
            ENode::BooleanValue(_) => Value::new_enode(true),
            _ => Not::new_enode(arg_eval),
        }
    }

    fn kind(&self) -> EKind {
        EKind::PureFunction
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct True {}
impl True {
    pub fn new_enode() -> ENode {
        ENode::BooleanTrue(True {})
    }
}
impl EType for True {
    fn args(&self) -> Option<&[ENode]> {
        None
    }

    fn eval(&self) -> ENode {
        Value::new_enode(true)
    }

    fn kind(&self) -> EKind {
        EKind::PureFunction
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Value {
    val: bool,
}
impl Value {
    pub fn new_enode(val: bool) -> ENode {
        ENode::BooleanValue(Value { val })
    }

    pub fn as_bool(&self) -> bool {
        self.val
    }
}
impl EType for Value {
    fn args(&self) -> Option<&[ENode]> {
        None
    }

    fn eval(&self) -> ENode {
        match self.val {
            false => Value::new_enode(true),
            true => Value::new_enode(false),
        }
    }

    fn kind(&self) -> EKind {
        EKind::Data
    }
}
