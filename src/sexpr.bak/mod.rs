mod name;
mod namespace;
pub mod parse;
pub mod serialize;

use crate::ENode;
use crate::EType;

// DUTH

pub(crate) const NAMESPACE: &str = "sexpr";
pub(crate) const ERROR: &str = "error";
pub(crate) const PARSE: &str = "parse";
pub(crate) const SERIALIZE: &str = "serialize";
pub(crate) const SEXPR: &str = "sexpr";

#[derive(Debug, Clone, PartialEq)]
pub enum Error {
    UnknownName(String),
    UnknownNamespace(String),
    // TODO go through these errors, improving names, payloads and deleteing unused ones.
    MissingArgs,
    TooManyArgs,
    MissingClosingSquareBracket,
    MissingStringTerminatorBeforeInputEnd,
    IllegalArgument,
    NonWhitespaceCharacterWhenLookingForEnd(char),
    MissingHexidecimalDigits,
    IllegalFirstQNameChar,
    IllegalQNameChar(char),
    MissingENodeEndBeforeInputEnd,
    IllegalQNameCharacter,
    IllegalCharForMode(char, parse::Mode),
    IllegalCharacterAfterPrimitiveInArgs(char),
    PrematureEnd,
}

pub(crate) fn parse(args: &[ENode]) -> ENode {
    if args.len() != 1 {
        panic!("parse can only accept 1 arg");
    }

    let s = match &args[0] {
        ENode::SExprSExpr(s) => s,
        _ => "TODO",
    };

    parse::parse_str(&s)
}

pub(crate) fn serialize(args: &[ENode]) -> ENode {
    if args.len() != 1 {
        panic!("serialize only accepts arg.len=1")
    }

    ENode::SExprSExpr(serialize::serialize_as_string(&args[0], ""))
}
