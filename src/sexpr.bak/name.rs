use crate::ENode;

pub(crate) fn name_as_str(enode: &ENode) -> &str {
    match enode {
        ENode::BooleanAnd(_) => crate::boolean::AND,
        ENode::BooleanFalse => crate::boolean::FALSE,
        ENode::BooleanNot(_) => crate::boolean::NOT,
        ENode::BooleanTrue => crate::boolean::TRUE,
        ENode::BooleanValue(_) => "value",

        ENode::MathMLMarkup(_) => crate::mathml::MARKUP,
        ENode::MathMLSerializeContent(_) => crate::mathml::SERIALIZE_CONTENT,
        ENode::MathMLSerializeParallel(_) => crate::mathml::SERIALIZE_PARALLEL,
        ENode::MathMLSerializePresentation(_) => crate::mathml::SERIALIZE_PRESENTATION,

        ENode::PrimitiveString(_) => crate::primitive::STRING,

        ENode::SExprError(_) => crate::sexpr::ERROR,
        ENode::SExprParse(_) => crate::sexpr::PARSE,
        ENode::SExprSerialize(_) => crate::sexpr::SERIALIZE,
        ENode::SExprSExpr(_) => crate::sexpr::SEXPR,
    }
}

pub(crate) fn name_from_qname(qname: &str) -> &str {
    match qname.rfind('/') {
        Some(i) => &qname[i + 1..],
        None => qname,
    }
}
