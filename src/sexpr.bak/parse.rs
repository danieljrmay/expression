use crate::sexpr::name;
use crate::sexpr::namespace;
use crate::sexpr::Error;
use crate::ENode;
use std::str::Chars;

pub fn parse_str(s: &str) -> ENode {
    let mut tokenizer = Tokenizer::new(s.chars());
    let mut args = parse_args(&mut tokenizer, "", true);

    match args.len() {
        0 => ENode::SExprError(Error::MissingArgs),
        1 => args.pop().unwrap(),
        _ => ENode::SExprError(Error::TooManyArgs),
    }
}

fn parse_args<'a>(tokenizer: &mut Tokenizer, current_namespace: &'a str, root: bool) -> Vec<ENode> {
    let mut enodes: Vec<ENode> = Vec::new();

    loop {
        match tokenizer.next() {
            None if root => break,
            None => enodes.push(ENode::SExprError(Error::MissingClosingSquareBracket)),
            Some(Token::UnicodeString(s)) => enodes.push(ENode::PrimitiveString(s)),
            Some(Token::ENodeEnd) => break,
            Some(Token::Error(error)) => enodes.push(ENode::SExprError(error)),
            Some(Token::ENodeStart(qname)) => {
                let namespace = match namespace::namespace_from_qname(&qname) {
                    Some(ns) => ns,
                    None => current_namespace,
                };
                let name = name::name_from_qname(&qname);

                let enode = match namespace {
                    crate::boolean::NAMESPACE => match name {
                        crate::boolean::AND => {
                            ENode::BooleanAnd(parse_args(tokenizer, namespace, false))
                        }
                        crate::boolean::NOT => {
                            ENode::BooleanNot(parse_args(tokenizer, namespace, false))
                        }
                        crate::boolean::TRUE => ENode::BooleanTrue,
                        crate::boolean::FALSE => ENode::BooleanFalse,
                        _ => ENode::SExprError(Error::UnknownName(name.to_string())),
                    },
                    crate::mathml::NAMESPACE => match name {
                        crate::mathml::MARKUP => {
                            if let Some(Token::UnicodeString(s)) = tokenizer.next() {
                                ENode::MathMLMarkup(s)
                            } else {
                                ENode::SExprError(Error::IllegalArgument)
                            }
                        }
                        crate::mathml::SERIALIZE_CONTENT => {
                            ENode::MathMLSerializeContent(parse_args(tokenizer, namespace, false))
                        }
                        crate::mathml::SERIALIZE_PARALLEL => {
                            ENode::MathMLSerializeParallel(parse_args(tokenizer, namespace, false))
                        }
                        crate::mathml::SERIALIZE_PRESENTATION => {
                            ENode::MathMLSerializePresentation(parse_args(
                                tokenizer, namespace, false,
                            ))
                        }
                        _ => ENode::SExprError(Error::UnknownName(name.to_string())),
                    },
                    _ => ENode::SExprError(Error::UnknownNamespace(namespace.to_string())),
                };

                enodes.push(enode)
            }
        }
    }

    enodes
}

#[cfg(test)]
mod parse_str {
    use super::*;

    #[test]
    fn nullary_enode() {
        assert_eq!(parse_str("[boolean/true]"), ENode::BooleanTrue);
    }

    #[test]
    fn unary_enode() {
        let mut t = Tokenizer::new("[a/b[c]]".chars());
        assert_eq!(t.next(), Some(Token::ENodeStart(String::from("a/b"))));
        assert_eq!(t.next(), Some(Token::ENodeStart(String::from("c"))));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("[boolean/not[true]]".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("boolean/not")))
        );
        assert_eq!(t.next(), Some(Token::ENodeStart(String::from("true"))));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);
    }
}

#[derive(Debug, PartialEq)]
pub(crate) enum Token {
    ENodeEnd,
    ENodeStart(String),
    Error(Error),
    UnicodeString(String),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Mode {
    Args,
    End,
    EnodeEnd,
    Error,
    QName,
    Start,
    UnicodeString,
}

pub(crate) struct Tokenizer<'a> {
    chars: Chars<'a>,
    mode_stack: Vec<Mode>,
}
impl<'a> Tokenizer<'a> {
    fn new(chars: Chars<'a>) -> Tokenizer<'a> {
        Tokenizer {
            chars,
            mode_stack: vec![Mode::Start],
        }
    }

    fn error(&mut self, e: Error) -> Option<Token> {
        self.mode_stack.push(Mode::Error);
        Some(Token::Error(e))
    }

    fn start(&mut self) -> Option<Token> {
        match self.next_non_whitespace_char() {
            Some('[') => {
                debug_assert_eq!(self.mode_stack.pop(), Some(Mode::Start));
                self.mode_stack.push(Mode::End);
                self.mode_stack.push(Mode::Args);
                self.mode_stack.push(Mode::QName);

                self.qname()
            }
            Some('`') => {
                debug_assert_eq!(self.mode_stack.pop(), Some(Mode::Start));
                self.mode_stack.push(Mode::End);
                self.mode_stack.push(Mode::UnicodeString);

                self.unicode_string()
            }
            Some(c) => self.error(Error::IllegalCharForMode(c, Mode::Start)),
            None => None,
        }
    }

    fn unicode_string(&mut self) -> Option<Token> {
        let mut s = String::new();

        loop {
            match self.chars.next() {
                Some('`') => {
                    debug_assert_eq!(self.mode_stack.pop(), Some(Mode::UnicodeString));
                    return Some(Token::UnicodeString(s));
                }
                Some(c) => s.push(c),
                None => return self.error(Error::MissingStringTerminatorBeforeInputEnd),
            }
        }
    }

    fn next_non_whitespace_char(&mut self) -> Option<char> {
        loop {
            match self.chars.next() {
                Some(c) if c.is_whitespace() => continue,
                Some(c) => return Some(c),
                None => return None,
            }
        }
    }

    fn end(&mut self) -> Option<Token> {
        match self.next_non_whitespace_char() {
            Some(c) => self.error(Error::NonWhitespaceCharacterWhenLookingForEnd(c)),
            None => None,
        }
    }

    fn qname(&mut self) -> Option<Token> {
        let mut s = String::new();

        match self.chars.next() {
            Some(c) if c.is_alphabetic() => s.push(c),
            _ => return self.error(Error::IllegalFirstQNameChar),
        }

        loop {
            match self.chars.next() {
                Some(c) if c.is_alphanumeric() => s.push(c),
                Some('/') => s.push('/'),
                Some('[') => {
                    debug_assert_eq!(self.mode_stack.pop(), Some(Mode::QName));
                    self.mode_stack.push(Mode::Args);
                    self.mode_stack.push(Mode::QName);
                    return Some(Token::ENodeStart(s));
                }
                Some('`') => {
                    debug_assert_eq!(self.mode_stack.pop(), Some(Mode::QName));
                    self.mode_stack.push(Mode::Args);
                    self.mode_stack.push(Mode::UnicodeString);
                    return Some(Token::ENodeStart(s));
                }
                Some(c) if c.is_whitespace() => {
                    debug_assert_eq!(self.mode_stack.pop(), Some(Mode::QName));
                    self.mode_stack.push(Mode::Args);
                    return Some(Token::ENodeStart(s));
                }
                Some(']') => {
                    debug_assert_eq!(self.mode_stack.pop(), Some(Mode::QName));
                    debug_assert_eq!(self.mode_stack.pop(), Some(Mode::Args));
                    self.mode_stack.push(Mode::EnodeEnd);
                    return Some(Token::ENodeStart(s));
                }
                Some(c) => return self.error(Error::IllegalQNameChar(c)),
                None => return self.error(Error::MissingENodeEndBeforeInputEnd),
            }
        }
    }

    fn enode_end(&mut self) -> Option<Token> {
        debug_assert_eq!(self.mode_stack.pop(), Some(Mode::EnodeEnd));

        Some(Token::ENodeEnd)
    }

    fn args(&mut self) -> Option<Token> {
        match self.next_non_whitespace_char() {
            Some(']') => {
                debug_assert_eq!(self.mode_stack.pop(), Some(Mode::Args));
                Some(Token::ENodeEnd)
            }
            Some('[') => {
                self.mode_stack.push(Mode::Args);
                self.mode_stack.push(Mode::QName);
                self.qname()
            }
            Some('`') => {
                self.mode_stack.push(Mode::UnicodeString);
                self.unicode_string()
            }
            Some(c) => self.error(Error::IllegalCharForMode(c, Mode::Args)),
            None => self.error(Error::MissingENodeEndBeforeInputEnd),
        }
    }
}
impl<'a> Iterator for Tokenizer<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        match self.mode_stack.last() {
            Some(Mode::Args) => self.args(),
            Some(Mode::End) => self.end(),
            Some(Mode::EnodeEnd) => self.enode_end(),
            Some(Mode::Error) => None,
            Some(Mode::QName) => self.qname(),
            Some(Mode::Start) => self.start(),
            Some(Mode::UnicodeString) => self.unicode_string(),
            None => {
                unreachable!("The mode stack should never be empty.");
            }
        }
    }
}

#[cfg(test)]
mod tokenizer {
    use super::*;

    #[test]
    fn nullary_enode() {
        let mut t = Tokenizer::new("[namespace/name]".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("namespace/name")))
        );
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("   [namespace/name]   ".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("namespace/name")))
        );
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);
    }

    #[test]
    fn unary_enode() {
        let mut t = Tokenizer::new("[a/b[c]]".chars());
        assert_eq!(t.next(), Some(Token::ENodeStart(String::from("a/b"))));
        assert_eq!(t.next(), Some(Token::ENodeStart(String::from("c"))));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("[boolean/not[true]]".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("boolean/not")))
        );
        assert_eq!(t.next(), Some(Token::ENodeStart(String::from("true"))));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);
    }
}
