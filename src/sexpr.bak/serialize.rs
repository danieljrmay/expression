use crate::sexpr::name::name_as_str;
use crate::sexpr::namespace::namespace_as_str;
use crate::ENode;

pub fn serialize_as_string(enode: &ENode, current_namespace: &str) -> String {
    let mut s = String::new();

    serialize_enode(&mut s, enode, current_namespace);

    s
}

pub(super) fn serialize_enode(w: &mut String, enode: &ENode, current_namespace: &str) {
    match enode {
        ENode::PrimitiveString(s) => serialize_str(w, s),
        ENode::MathMLMarkup(s) => {
            serialize_node_head(w, enode, current_namespace);
            serialize_str(w, s);
            serialize_node_tail(w);
        }
        ENode::SExprError(e) => w.push_str(&format!("[sexpr/error {:?}]", e)),
        _ => {
            w.push('[');

            let namespace = namespace_as_str(enode);
            if namespace != current_namespace {
                w.push_str(namespace);
                w.push('/');
            }

            w.push_str(name_as_str(enode));

            for arg in enode.args() {
                serialize_enode(w, arg, namespace);
            }

            serialize_node_tail(w);
        }
    }
}

fn serialize_node_head(w: &mut String, enode: &ENode, current_namespace: &str) {
    w.push('[');

    let namespace = namespace_as_str(enode);
    if namespace != current_namespace {
        w.push_str(namespace);
        w.push('/');
    }

    w.push_str(name_as_str(enode));
}

fn serialize_node_tail(w: &mut String) {
    w.push(']');
}

fn serialize_str(w: &mut String, s: &str) {
    w.push('`');
    w.push_str(s);
    w.push('`');
}
