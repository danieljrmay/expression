#[derive(Debug, PartialEq)]
pub(crate) enum Token {
    Boolean(bool),
    Byte(u8),
    String(String),
    U64(u64),
    U128(u128),
    ENodeStart(String),
    ENodeEnd,
    Error(TokenizerError),
}

#[derive(Debug, PartialEq)]
enum TokenizerError {
    NonWhitespaceCharacterWhenLookingForEnd(char),
    MissingHexidecimalDigits,
    ParseIntError(ParseIntError),
    MissingStringTerminatorBeforeInputEnd,
    IllegalFirstQNameChar,
    IllegalQNameChar(char),
    MissingENodeEndBeforeInputEnd,
    IllegalQNameCharacter,
    IllegalCharForMode(char, Mode),
    IllegalCharacterAfterPrimitiveInArgs(char),
    PrematureEnd,
}

#[derive(Debug, PartialEq)]
enum Mode {
    Start,
    End,
    Args,
    EnodeEnd,
    Error,
    RequireWhitespaceOrEnodeEnd,
}

pub(crate) struct Tokenizer<'a> {
    chars: Chars<'a>,
    mode_stack: Vec<Mode>,
    buffer: Option<char>,
}
impl<'a> Tokenizer<'a> {
    fn new(chars: Chars<'a>) -> Tokenizer<'a> {
        Tokenizer {
            chars,
            mode_stack: vec![Mode::Start],
            buffer: None,
        }
    }

    fn replace_mode(&mut self, new_mode: Mode) {
        self.mode_stack.pop();
        self.mode_stack.push(new_mode);
    }

    fn error(&mut self, te: TokenizerError) -> Option<Token> {
        self.replace_mode(Mode::Error);
        Some(Token::Error(te))
    }

    fn start(&mut self) -> Option<Token> {
        match self.next_non_whitespace_char() {
            Some('f') => {
                self.replace_mode(Mode::End);
                Some(Token::Boolean(false))
            }
            Some('t') => {
                self.replace_mode(Mode::End);
                Some(Token::Boolean(true))
            }
            Some('#') => {
                self.replace_mode(Mode::End);
                self.byte()
            }
            Some('`') => {
                self.replace_mode(Mode::End);
                self.string()
            }
            Some('[') => {
                self.replace_mode(Mode::End);
                self.qname()
            }
            Some(c) if c.is_numeric() => {
                unimplemented!("Numeric primitives have not been implemented yet.")
            }
            Some(c) => self.error(TokenizerError::IllegalCharForMode(c, Mode::Start)),
            None => None,
        }
    }

    fn next_non_whitespace_char(&mut self) -> Option<char> {
        loop {
            match self.chars.next() {
                Some(c) if c.is_whitespace() => continue,
                Some(c) => return Some(c),
                None => return None,
            }
        }
    }

    fn end(&mut self) -> Option<Token> {
        match self.next_non_whitespace_char() {
            Some(c) => self.error(TokenizerError::NonWhitespaceCharacterWhenLookingForEnd(c)),
            None => None,
        }
    }

    fn byte(&mut self) -> Option<Token> {
        let mut s = String::new();

        match self.chars.next() {
            Some(c) if c.is_ascii_hexdigit() => s.push(c),
            _ => return self.error(TokenizerError::MissingHexidecimalDigits),
        }

        match self.chars.next() {
            Some(c) if c.is_ascii_hexdigit() => {
                s.push(c);

                match u8::from_str_radix(&s, 16) {
                    Ok(u) => Some(Token::Byte(u)),
                    Err(e) => self.error(TokenizerError::ParseIntError(e)),
                }
            }
            _ => self.error(TokenizerError::MissingHexidecimalDigits),
        }
    }

    fn string(&mut self) -> Option<Token> {
        let mut s = String::new();

        loop {
            match self.chars.next() {
                Some('`') => return Some(Token::String(s)),
                Some(c) => s.push(c),
                None => return self.error(TokenizerError::MissingStringTerminatorBeforeInputEnd),
            }
        }
    }

    fn qname(&mut self) -> Option<Token> {
        let mut s = String::new();

        match self.chars.next() {
            Some(c) if c.is_alphabetic() => s.push(c),
            _ => return self.error(TokenizerError::IllegalFirstQNameChar),
        }

        loop {
            match self.chars.next() {
                Some(c) if c.is_alphanumeric() => s.push(c),
                Some('/') => s.push('/'),
                Some(c) if c.is_whitespace() => {
                    self.mode_stack.push(Mode::Args);
                    return Some(Token::ENodeStart(s));
                }
                Some(']') => {
                    self.mode_stack.push(Mode::EnodeEnd);
                    return Some(Token::ENodeStart(s));
                }
                Some(c) => return self.error(TokenizerError::IllegalQNameChar(c)),

                None => return self.error(TokenizerError::MissingENodeEndBeforeInputEnd),
            }
        }
    }

    fn enode_end(&mut self) -> Option<Token> {
        self.mode_stack.pop();

        Some(Token::ENodeEnd)
    }

    fn require_whitespace_or_enode_end(&mut self) -> Option<Token> {
        match self.chars.next() {
            Some(c) if c.is_whitespace() => {
                self.mode_stack.pop();

                self.next()
            }
            Some(']') => {
                self.replace_mode(Mode::EnodeEnd);

                self.next()
            }
            Some(c) => self.error(TokenizerError::IllegalCharacterAfterPrimitiveInArgs(c)),
            None => self.error(TokenizerError::PrematureEnd),
        }
    }

    fn args(&mut self) -> Option<Token> {
        match self.next_non_whitespace_char() {
            Some(']') => {
                self.mode_stack.pop();

                Some(Token::ENodeEnd)
            }
            Some('f') => {
                self.mode_stack.push(Mode::RequireWhitespaceOrEnodeEnd);

                Some(Token::Boolean(false))
            }
            Some('t') => {
                self.mode_stack.push(Mode::RequireWhitespaceOrEnodeEnd);

                Some(Token::Boolean(true))
            }
            Some('#') => {
                self.mode_stack.push(Mode::RequireWhitespaceOrEnodeEnd);

                self.byte()
            }
            Some('`') => {
                self.mode_stack.push(Mode::RequireWhitespaceOrEnodeEnd);

                self.string()
            }
            Some('[') => self.qname(),
            Some(c) if c.is_numeric() => {
                unimplemented!("Numeric primitives have not been implemented yet.")
            }
            Some(c) => self.error(TokenizerError::IllegalCharForMode(c, Mode::Args)),
            None => None,
        }
    }
}
impl<'a> Iterator for Tokenizer<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        match self.mode_stack.last() {
            Some(Mode::Start) => self.start(),
            Some(Mode::End) => self.end(),
            Some(Mode::Args) => self.args(),
            Some(Mode::EnodeEnd) => self.enode_end(),
            Some(Mode::RequireWhitespaceOrEnodeEnd) => self.require_whitespace_or_enode_end(),
            Some(Mode::Error) => None,
            None => {
                unreachable!("The mode stack should never be empty.");
            }
        }
    }
}

#[cfg(test)]
mod tokenizer {
    use super::*;

    #[test]
    fn primitive_bit() {
        let mut t = Tokenizer::new("t".chars());
        assert_eq!(t.next(), Some(Token::Boolean(true)));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("   t   ".chars());
        assert_eq!(t.next(), Some(Token::Boolean(true)));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("f".chars());
        assert_eq!(t.next(), Some(Token::Boolean(false)));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("   f   ".chars());
        assert_eq!(t.next(), Some(Token::Boolean(false)));
        assert_eq!(t.next(), None);
    }

    #[test]
    fn primitive_byte() {
        let mut t = Tokenizer::new("#0F".chars());
        assert_eq!(t.next(), Some(Token::Byte(15)));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("   #0f   ".chars());
        assert_eq!(t.next(), Some(Token::Byte(15)));
        assert_eq!(t.next(), None);
    }

    #[test]
    fn primitive_string() {
        let mut t = Tokenizer::new("`abc123`".chars());
        assert_eq!(t.next(), Some(Token::String(String::from("abc123"))));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("   `abc123`   ".chars());
        assert_eq!(t.next(), Some(Token::String(String::from("abc123"))));
        assert_eq!(t.next(), None);
    }

    #[test]
    fn nullary_enode() {
        let mut t = Tokenizer::new("[namespace/name]".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("namespace/name")))
        );
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("   [namespace/name]   ".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("namespace/name")))
        );
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);
    }

    #[test]
    fn unary_enode() {
        let mut t = Tokenizer::new("[namespace/name t]".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("namespace/name")))
        );
        assert_eq!(t.next(), Some(Token::Boolean(true)));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("  [namespace/name  t  ]  ".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("namespace/name")))
        );
        assert_eq!(t.next(), Some(Token::Boolean(true)));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);
    }

    #[test]
    fn binary_enode() {
        let mut t = Tokenizer::new("[namespace/name t f]".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("namespace/name")))
        );
        assert_eq!(t.next(), Some(Token::Boolean(true)));
        assert_eq!(t.next(), Some(Token::Boolean(false)));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);

        let mut t = Tokenizer::new("  [namespace/name   t    f   ]   ".chars());
        assert_eq!(
            t.next(),
            Some(Token::ENodeStart(String::from("namespace/name")))
        );
        assert_eq!(t.next(), Some(Token::Boolean(true)));
        assert_eq!(t.next(), Some(Token::Boolean(false)));
        assert_eq!(t.next(), Some(Token::ENodeEnd));
        assert_eq!(t.next(), None);
    }
}
