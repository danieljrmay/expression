use crate::ENode;

pub(crate) fn namespace_as_str(enode: &ENode) -> &str {
    match enode {
        ENode::BooleanAnd(_) => crate::boolean::NAMESPACE,
        ENode::BooleanFalse => crate::boolean::NAMESPACE,
        ENode::BooleanNot(_) => crate::boolean::NAMESPACE,
        ENode::BooleanTrue => crate::boolean::NAMESPACE,
        ENode::BooleanValue(_) => crate::boolean::NAMESPACE,

        ENode::MathMLMarkup(_) => crate::mathml::NAMESPACE,
        ENode::MathMLSerializeContent(_) => crate::mathml::NAMESPACE,
        ENode::MathMLSerializeParallel(_) => crate::mathml::NAMESPACE,
        ENode::MathMLSerializePresentation(_) => crate::mathml::NAMESPACE,

        ENode::PrimitiveString(_) => crate::primitive::NAMESPACE,

        ENode::SExprParse(_) => crate::sexpr::NAMESPACE,
        ENode::SExprSerialize(_) => crate::sexpr::NAMESPACE,
        ENode::SExprSExpr(_) => crate::sexpr::NAMESPACE,
        ENode::SExprError(_) => crate::sexpr::NAMESPACE,
    }
}

pub(crate) fn namespace_from_qname(qname: &str) -> Option<&str> {
    match qname.rfind('/') {
        Some(i) => Some(&qname[0..i]),
        None => None,
    }
}
