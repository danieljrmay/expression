use crate::ENode;

pub(crate) fn serialize(s: &mut String, enode: &ENode) {
    match enode {
        ENode::PrimitiveString(_) => {}
        ENode::BooleanAnd(args) => {
            s.push_str("<apply><and/>");

            for a in args {
                serialize(s, a);
            }

            s.push_str("</apply>");
        }
        ENode::BooleanFalse => s.push_str("<false/>"),
        ENode::BooleanNot(args) => {
            s.push_str("<apply><not/>");

            if args.len() == 1 {
                serialize(s, &args[0]);
            } else {
                panic!("boolean/not only accepts 1 argument.");
            }

            s.push_str("</apply>");
        }
        ENode::BooleanTrue => s.push_str("<true/>"),
        ENode::BooleanValue(v) => {
            if v.as_bool() {
                s.push_str("<true/>");
            } else {
                s.push_str("<false/>");
            }
        }
        ENode::MathMLMarkup(_) => {}
        ENode::MathMLSerializeContent(_) => {}
        ENode::MathMLSerializeParallel(_) => {}
        ENode::MathMLSerializePresentation(_) => {}
        ENode::SExprError(_) => {}
        ENode::SExprParse(_) => {}
        ENode::SExprSerialize(_) => {}
        ENode::SExprSExpr(_) => {}
    }
}
