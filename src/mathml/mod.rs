mod content;
mod presentation;

use crate::ENode;

pub const NAMESPACE: &str = "mathml";
pub const MARKUP: &str = "markup";
pub const SERIALIZE_CONTENT: &str = "serializecontent";
pub const SERIALIZE_PARALLEL: &str = "serializeparallel";
pub const SERIALIZE_PRESENTATION: &str = "serializepresentation";

const MATH_STAG: &str = "<math xmlns='http://www.w3.org/1998/Math/MathML'>";
const MATH_ETAG: &str = "</math>";
const PARALLEL_HEAD: &str = "<math xmlns='http://www.w3.org/1998/Math/MathML'><sematics>"; // TODO can we not use MATH_STAG, see const_format module
const PARALLEL_TAIL: &str = "</annotation-xml></sematics></math>"; // TODO can we not use MATH_ETAG, see const_format module

pub(crate) fn serialize_content(args: &[ENode]) -> ENode {
    let mut markup = String::from(MATH_STAG);

    for arg in args {
        content::serialize(&mut markup, arg);
    }

    markup.push_str(MATH_ETAG);

    ENode::MathMLMarkup(markup)
}

pub(crate) fn serialize_parallel(args: &[ENode]) -> ENode {
    let mut markup = String::from(PARALLEL_HEAD);

    for arg in args {
        presentation::serialize(&mut markup, arg);
    }

    markup.push_str("<annotation-xml encoding='MathML-Content'>");

    for arg in args {
        content::serialize(&mut markup, arg);
    }

    markup.push_str(PARALLEL_TAIL);

    ENode::MathMLMarkup(markup)
}

pub(crate) fn serialize_presentation(args: &[ENode]) -> ENode {
    let mut markup = String::from(MATH_STAG);

    for arg in args {
        presentation::serialize(&mut markup, arg);
    }

    markup.push_str(MATH_ETAG);

    ENode::MathMLMarkup(markup)
}

#[cfg(test)]
mod serialize {
    use super::*;

    #[test]
    fn content() {
        let enode =
            ENode::MathMLSerializeContent(vec![ENode::BooleanNot(vec![ENode::BooleanFalse])]);
        assert_eq!(enode.eval(), ENode::MathMLMarkup(String::from("<math xmlns='http://www.w3.org/1998/Math/MathML'><apply><not/><false/></apply></math>")));
    }

    #[test]
    fn parallel() {
        let enode =
            ENode::MathMLSerializeParallel(vec![ENode::BooleanNot(vec![ENode::BooleanFalse])]);
        assert_eq!(enode.eval(), ENode::MathMLMarkup(String::from("<math xmlns='http://www.w3.org/1998/Math/MathML'><sematics><mrow><mo>¬</mo><mi>false</mi></mrow><annotation-xml encoding='MathML-Content'><apply><not/><false/></apply></annotation-xml></sematics></math>")));
    }

    #[test]
    fn presentation() {
        let enode =
            ENode::MathMLSerializePresentation(vec![ENode::BooleanNot(vec![ENode::BooleanFalse])]);
        assert_eq!(enode.eval(), ENode::MathMLMarkup(String::from("<math xmlns='http://www.w3.org/1998/Math/MathML'><mrow><mo>¬</mo><mi>false</mi></mrow></math>")));
    }
}
