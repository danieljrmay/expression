use crate::ENode;

pub(crate) fn serialize(s: &mut String, enode: &ENode) {
    match enode {
        ENode::PrimitiveString(_) => {}
        ENode::BooleanAnd(args) => {
            s.push_str("<mrow>");

            let n = args.len();

            for i in 0..n - 2 {
                serialize(s, &args[i]);
                s.push_str("<mo>∧</mo>")
            }

            serialize(s, &args[n - 1]);

            s.push_str("</mrow>");
        }
        ENode::BooleanFalse => s.push_str("<mi>false</mi>"),
        ENode::BooleanNot(args) => {
            s.push_str("<mrow><mo>¬</mo>");

            if args.len() == 1 {
                serialize(s, &args[0]);
            } else {
                panic!("boolean/not only accepts 1 argument.");
            }

            s.push_str("</mrow>");
        }
        ENode::BooleanTrue => s.push_str("<mi>true</mi>"),
        ENode::BooleanValue(v) => {
            if v.as_bool() {
                s.push_str("<mi>true</mi>")
            } else {
                s.push_str("<mi>false</mi>")
            }
        }

        ENode::MathMLMarkup(_) => {}
        ENode::MathMLSerializeContent(_) => {}
        ENode::MathMLSerializeParallel(_) => {}
        ENode::MathMLSerializePresentation(_) => {}
        ENode::SExprError(_) => {}
        ENode::SExprParse(_) => {}
        ENode::SExprSerialize(_) => {}
        ENode::SExprSExpr(_) => {}
    }
}
