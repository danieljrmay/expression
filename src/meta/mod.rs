use crate::EKind;
use crate::ENode;
use crate::EType;
use std::fmt;

#[derive(Debug, Clone, PartialEq)]
pub struct Error {
    args: Vec<ENode>,
}
impl Error {
    pub fn new_enode(message: String, mut args: Vec<ENode>) -> ENode {
        // TODO change return type to Result<ENode,ENode>
        args.push(Message::new_enode(message));

        ENode::MetaError(Error { args })
    }
}
impl EType for Error {
    fn args(&self) -> Option<&[ENode]> {
        Some(&self.args)
    }

    fn eval(&self) -> ENode {
        ENode::MetaError(self.clone())
    }

    fn kind(&self) -> EKind {
        EKind::PureFunction
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Message {
    val: String,
}
impl Message {
    pub fn new_enode(val: String) -> ENode {
        ENode::MetaMessage(Message { val })
    }
}
impl EType for Message {
    fn args(&self) -> Option<&[ENode]> {
        None
    }

    fn eval(&self) -> ENode {
        Message::new_enode(self.val.clone())
    }

    fn kind(&self) -> EKind {
        EKind::Data
    }
}
impl fmt::Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.val)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Placeholder {
    val: String,
}
impl Placeholder {
    pub fn new_enode(val: String) -> ENode {
        ENode::MetaPlaceholder(Placeholder { val })
    }
}
impl EType for Placeholder {
    fn args(&self) -> Option<&[ENode]> {
        None
    }

    fn eval(&self) -> ENode {
        Placeholder::new_enode(self.val.clone())
    }

    fn kind(&self) -> EKind {
        EKind::Data
    }
}
impl fmt::Display for Placeholder {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.val)
    }
}
