mod boolean;
mod meta;
mod sexpr;
//mod mathml;
//mod primitive;
//pub mod sexpr;

pub enum EKind {
    Data,
    PureFunction,
}

#[derive(Debug, Clone, PartialEq)]
pub enum ENode {
    BooleanAnd(boolean::And),
    BooleanFalse(boolean::False),
    BooleanNot(boolean::Not),
    BooleanTrue(boolean::True),
    BooleanValue(boolean::Value),
    // MathMLMarkup(String),
    // MathMLSerializeContent(Vec<ENode>),
    // MathMLSerializeParallel(Vec<ENode>),
    // MathMLSerializePresentation(Vec<ENode>),
    MetaError(meta::Error),
    MetaMessage(meta::Message),
    MetaPlaceholder(meta::Placeholder),
    // PrimitiveString(String),
    SExprENodeName(sexpr::ENodeName),
    SExprENodeFQName(sexpr::ENodeFQName),
    SExprENodeSpace(sexpr::ENodeSpace),
    SExprFQName(sexpr::FQName),
    SExprName(sexpr::Name),
    SExprSerialize(sexpr::Serialize),
    SExprSExpression(sexpr::SExpression),
    SExprSpace(sexpr::Space),
    // SExprError(sexpr::Error),
    // SExprParse(Vec<ENode>),
    // SExprSExpr(String),
}
impl ENode {
    pub fn args(&self) -> Option<&[ENode]> {
        self.etype().args()
    }

    pub fn etype(&self) -> &dyn EType {
        match self {
            ENode::BooleanAnd(t) => t,
            ENode::BooleanFalse(t) => t,
            ENode::BooleanNot(t) => t,
            ENode::BooleanTrue(t) => t,
            ENode::BooleanValue(t) => t,

            ENode::MetaError(t) => t,
            ENode::MetaMessage(t) => t,
            ENode::MetaPlaceholder(t) => t,

            ENode::SExprFQName(t) => t,
            ENode::SExprName(t) => t,
            ENode::SExprENodeFQName(t) => t,
            ENode::SExprENodeName(t) => t,
            ENode::SExprENodeSpace(t) => t,
            ENode::SExprSerialize(t) => t,
            ENode::SExprSExpression(t) => t,
            ENode::SExprSpace(t) => t,
        }
    }

    pub fn eval(&self) -> ENode {
        self.etype().eval()
    }

    pub fn kind(&self) -> EKind {
        self.etype().kind()
    }
}

pub trait EType {
    fn args(&self) -> Option<&[ENode]>;
    fn kind(&self) -> EKind;
    fn eval(&self) -> ENode;
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem;

    #[test]
    fn size() {
        assert_eq!(mem::size_of::<ENode>(), 32);
    }

    #[test]
    fn smoke() {
        let not = boolean::Not::new_enode(boolean::False::new_enode());
        assert_eq!(not.eval(), boolean::Value::new_enode(true));

        let serialize = sexpr::Serialize::new_enode(not);
        assert_eq!(
            serialize.eval(),
            sexpr::SExpression::new_enode(String::from("[boolean/not [false]]"))
        );
    }
}
