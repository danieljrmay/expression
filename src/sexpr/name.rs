use super::ENodeName;

pub const AND: ENodeName = ENodeName::new("and");
pub const FALSE: ENodeName = ENodeName::new("false");
pub const NOT: ENodeName = ENodeName::new("not");
pub const TRUE: ENodeName = ENodeName::new("true");
pub const VALUE: ENodeName = ENodeName::new("value");
pub const ERROR: ENodeName = ENodeName::new("error");
pub const MESSAGE: ENodeName = ENodeName::new("message");
pub const PLACEHOLDER: ENodeName = ENodeName::new("placeholder");
pub const ENODEFQNAME: ENodeName = ENodeName::new("enodefqname");
pub const ENODENAME: ENodeName = ENodeName::new("enodename");
pub const ENODESPACE: ENodeName = ENodeName::new("enodespace");
pub const FQNAME: ENodeName = ENodeName::new("fqname");
pub const NAME: ENodeName = ENodeName::new("name");
pub const SERIALIZE: ENodeName = ENodeName::new("serialize");
pub const SEXPRESSION: ENodeName = ENodeName::new("sexpression");
pub const SPACE: ENodeName = ENodeName::new("space");
