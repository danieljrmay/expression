mod fqname;
mod name;
mod space;

use crate::EKind;
use crate::ENode;
use crate::EType;
use std::fmt;

#[derive(Debug, Clone, PartialEq)]
pub struct ENodeFQName {
    space: &'static ENodeSpace,
    name: &'static ENodeName,
}
impl ENodeFQName {
    pub const fn new(space: &'static ENodeSpace, name: &'static ENodeName) -> ENodeFQName {
        ENodeFQName { space, name }
    }
}
impl EType for ENodeFQName {
    fn args(&self) -> Option<&[ENode]> {
        None
    }

    fn kind(&self) -> EKind {
        EKind::Data
    }

    fn eval(&self) -> ENode {
        ENode::from(ENodeFQName::new(self.space, self.name))
    }
}
impl From<&ENode> for ENodeFQName {
    fn from(enode: &ENode) -> Self {
        match enode {
            ENode::BooleanAnd(_) => fqname::BOOLEAN_AND,
            ENode::BooleanFalse(_) => fqname::BOOLEAN_FALSE,
            ENode::BooleanNot(_) => fqname::BOOLEAN_NOT,
            ENode::BooleanTrue(_) => fqname::BOOLEAN_TRUE,
            ENode::BooleanValue(_) => fqname::BOOLEAN_VALUE,

            ENode::MetaError(_) => fqname::META_ERROR,
            ENode::MetaMessage(_) => fqname::META_MESSAGE,
            ENode::MetaPlaceholder(_) => fqname::META_PLACEHOLDER,

            ENode::SExprENodeFQName(_) => fqname::SEXPR_ENODEFQNAME,
            ENode::SExprENodeName(_) => fqname::SEXPR_ENODENAME,
            ENode::SExprENodeSpace(_) => fqname::SEXPR_ENODESPACE,
            ENode::SExprFQName(_) => fqname::SEXPR_FQNAME,
            ENode::SExprName(_) => fqname::SEXPR_NAME,
            ENode::SExprSerialize(_) => fqname::SEXPR_SERIALIZE,
            ENode::SExprSExpression(_) => fqname::SEXPR_SEXPRESSION,
            ENode::SExprSpace(_) => fqname::SEXPR_SPACE,
        }
    }
}
impl From<ENodeFQName> for ENode {
    // TODO replace with new_enode() ?
    fn from(etype: ENodeFQName) -> Self {
        ENode::SExprENodeFQName(etype)
    }
}
impl fmt::Display for ENodeFQName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}/{}", self.space, self.name)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct ENodeName {
    val: &'static str,
}
impl ENodeName {
    pub const fn new(val: &'static str) -> ENodeName {
        ENodeName { val }
    }
}
impl EType for ENodeName {
    fn args(&self) -> Option<&[ENode]> {
        None
    }

    fn kind(&self) -> EKind {
        EKind::Data
    }

    fn eval(&self) -> ENode {
        ENode::from(ENodeName::new(self.val))
    }
}
impl From<&ENode> for ENodeName {
    fn from(enode: &ENode) -> Self {
        match enode {
            ENode::BooleanAnd(_) => name::AND,
            ENode::BooleanFalse(_) => name::FALSE,
            ENode::BooleanNot(_) => name::NOT,
            ENode::BooleanTrue(_) => name::TRUE,
            ENode::BooleanValue(_) => name::VALUE,

            ENode::MetaError(_) => name::ERROR,
            ENode::MetaMessage(_) => name::MESSAGE,
            ENode::MetaPlaceholder(_) => name::PLACEHOLDER,

            ENode::SExprENodeFQName(_) => name::ENODEFQNAME,
            ENode::SExprENodeName(_) => name::ENODENAME,
            ENode::SExprENodeSpace(_) => name::ENODESPACE,
            ENode::SExprFQName(_) => name::FQNAME,
            ENode::SExprName(_) => name::NAME,
            ENode::SExprSerialize(_) => name::SERIALIZE,
            ENode::SExprSExpression(_) => name::SEXPRESSION,
            ENode::SExprSpace(_) => name::SPACE,
        }
    }
}
impl From<ENodeName> for ENode {
    fn from(etype: ENodeName) -> Self {
        ENode::SExprENodeName(etype)
    }
}
impl fmt::Display for ENodeName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.val)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct ENodeSpace {
    val: &'static str,
}
impl ENodeSpace {
    pub const fn new(val: &'static str) -> ENodeSpace {
        ENodeSpace { val }
    }
}
impl EType for ENodeSpace {
    fn args(&self) -> Option<&[ENode]> {
        None
    }

    fn kind(&self) -> EKind {
        EKind::Data
    }

    fn eval(&self) -> ENode {
        ENode::from(ENodeSpace::new(self.val))
    }
}
impl From<&ENode> for ENodeSpace {
    fn from(enode: &ENode) -> Self {
        match enode {
            ENode::BooleanAnd(_)
            | ENode::BooleanFalse(_)
            | ENode::BooleanNot(_)
            | ENode::BooleanTrue(_)
            | ENode::BooleanValue(_) => space::BOOLEAN,

            ENode::MetaError(_) | ENode::MetaMessage(_) | ENode::MetaPlaceholder(_) => space::META,

            ENode::SExprENodeFQName(_)
            | ENode::SExprENodeName(_)
            | ENode::SExprENodeSpace(_)
            | ENode::SExprFQName(_)
            | ENode::SExprName(_)
            | ENode::SExprSerialize(_)
            | ENode::SExprSExpression(_)
            | ENode::SExprSpace(_) => space::SEXPR,
        }
    }
}
impl From<ENodeSpace> for ENode {
    fn from(etype: ENodeSpace) -> Self {
        ENode::SExprENodeSpace(etype)
    }
}
impl fmt::Display for ENodeSpace {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.val)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct FQName {
    arg: Box<[ENode; 1]>,
}
impl FQName {
    pub fn new_enode(arg: ENode) -> ENode {
        ENode::SExprFQName(FQName {
            arg: Box::new([arg]),
        })
    }
}
impl EType for FQName {
    fn args(&self) -> Option<&[ENode]> {
        Some(self.arg.as_ref())
    }

    fn eval(&self) -> ENode {
        ENode::from(ENodeFQName::from(&self.arg[0]))
    }

    fn kind(&self) -> EKind {
        EKind::PureFunction
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Name {
    arg: Box<[ENode; 1]>,
}
impl Name {
    pub fn new_enode(arg: ENode) -> ENode {
        ENode::SExprName(Name {
            arg: Box::new([arg]),
        })
    }
}
impl EType for Name {
    fn args(&self) -> Option<&[ENode]> {
        Some(self.arg.as_ref())
    }

    fn eval(&self) -> ENode {
        ENode::from(ENodeName::from(&self.arg[0]))
    }

    fn kind(&self) -> EKind {
        EKind::PureFunction
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Space {
    arg: Box<[ENode; 1]>,
}
impl Space {
    pub fn new_enode(arg: ENode) -> ENode {
        ENode::SExprSpace(Space {
            arg: Box::new([arg]),
        })
    }
}
impl EType for Space {
    fn args(&self) -> Option<&[ENode]> {
        Some(self.arg.as_ref())
    }

    fn eval(&self) -> ENode {
        ENode::from(ENodeSpace::from(&self.arg[0]))
    }

    fn kind(&self) -> EKind {
        EKind::PureFunction
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct SExpression {
    val: String,
}
impl SExpression {
    pub fn new(val: String) -> SExpression {
        SExpression { val }
    }

    pub fn new_enode(val: String) -> ENode {
        ENode::SExprSExpression(SExpression::new(val))
    }
}
impl EType for SExpression {
    fn args(&self) -> Option<&[ENode]> {
        None
    }

    fn kind(&self) -> EKind {
        EKind::Data
    }

    fn eval(&self) -> ENode {
        SExpression::new_enode(self.val.clone())
    }
}
impl fmt::Display for SExpression {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.val)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Serialize {
    arg: Box<[ENode; 1]>,
}
impl Serialize {
    pub fn new_enode(arg: ENode) -> ENode {
        ENode::SExprSerialize(Serialize {
            arg: Box::new([arg]),
        })
    }

    fn serialize_enode(
        w: &mut dyn fmt::Write,
        enode: &ENode,
        current_space: &ENodeSpace,
    ) -> Result<(), fmt::Error> {
        let enode_space = ENodeSpace::from(enode);

        if current_space == &enode_space {
            write!(w, "[{}", ENodeName::from(enode))?;
        } else {
            write!(w, "[{}/{}", enode_space, ENodeName::from(enode))?;
        }

        match enode.kind() {
            EKind::Data => Serialize::serialize_data_enode(w, enode)?,
            EKind::PureFunction => {
                if let Some(args) = enode.args() {
                    for a in args {
                        w.write_char(' ')?;
                        Serialize::serialize_enode(w, a, &enode_space)?;
                    }
                }
            }
        }

        w.write_char(']')
    }

    fn serialize_data_enode(w: &mut dyn fmt::Write, enode: &ENode) -> Result<(), fmt::Error> {
        match enode {
            ENode::BooleanValue(t) if t.as_bool() => write!(w, " t"),
            ENode::BooleanValue(t) if !t.as_bool() => write!(w, " f"), //TODO replace with Display impl?

            ENode::MetaMessage(t) => write!(w, " `{}`", t),
            ENode::MetaPlaceholder(t) => write!(w, " `{}`", t),

            ENode::SExprENodeFQName(t) => write!(w, " `{}`", t),
            ENode::SExprENodeName(t) => write!(w, " `{}`", t),
            ENode::SExprENodeSpace(t) => write!(w, " `{}`", t),
            ENode::SExprSExpression(t) => write!(w, " `{}`", t),
            _ => unreachable!(),
        }
    }
}
impl EType for Serialize {
    fn args(&self) -> Option<&[ENode]> {
        Some(self.arg.as_ref())
    }

    fn eval(&self) -> ENode {
        let mut s = String::new();

        match Serialize::serialize_enode(&mut s, &self.arg[0], &space::NULL) {
            Ok(()) => SExpression::new_enode(s),
            Err(_e) => SExpression::new_enode(String::from("ERROR TODO")),
        }
    }

    fn kind(&self) -> EKind {
        EKind::PureFunction
    }
}
