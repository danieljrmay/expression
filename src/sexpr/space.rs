use super::ENodeSpace;

pub const BOOLEAN: ENodeSpace = ENodeSpace::new("boolean");
pub const META: ENodeSpace = ENodeSpace::new("meta");
pub const NULL: ENodeSpace = ENodeSpace::new("");
pub const SEXPR: ENodeSpace = ENodeSpace::new("sexpr");
