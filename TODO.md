# expression Todo List #

* [x] Add minimal MathML functionality.
* [x] Add String primitive.
* [ ] Add Boolean Value primitive.
* [ ] Change data types to structs, and functions to empty enums.
* [ ] Move name, namespace, fqdn, etc to sexpr module. 
* [ ] Update EType trait, consider EFunction, EData traits or an EKind
      enumeration.
* [ ] Improve tokenizer errors.
* [ ] Improve tokenizer and simplify whitespace handling by removing
      special methods.
* [ ] Add WebAssembly bridge.
* [ ] Decide how to handle input validation. For example we can
      currently enter `[mathml/markup 'INVALID']` how do we want to
      handle this?
* [ ] Consider replacing primitives of the form `NsName(String)` with
      `NsName(ENode impl UnicodeString)` or `NsName(Vec<ENode impl
      UnicodeString>)`, where `UnicodeString` is a type.
